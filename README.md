# README #

This README would normally document whatever steps are necessary to get your application up and running.

## What is this repository for? ##

* Quick summary: This repo is for creating an EKS cluster with a combination of terraform and CloudFormation tempaltes.
* Version: N/A
* [Instructions](https://github.com/Plan-A-Technologies/DevOps-Challenge-Level-2)

### How do I get set up? ###

* terraform init
* terraform plan -out terraform.tfplan
* terraform apply "terraform.tfplan"
* verify what was created in the CloudFormation console
* run the ./get-credentials.sh script to get the kubeconfig file

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
