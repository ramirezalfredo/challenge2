resource "aws_cloudformation_stack" "eks-worker-group" {
  name = "ChallengeWorkers"

  parameters = {
    ClusterName = "ChallengeEKS"
    KubernetesVersion = "1.21"
    Subnets = aws_cloudformation_stack.network.outputs.WorkerSubnetIds
    KeyName = "Alfredo-Mac"
    NodeInstanceType = "t3a.large"
    NodeAutoScalingGroupDesiredCapacity = "2"
    NodeAutoScalingGroupMaxSize = "2"
    NodeAutoScalingGroupMinSize = "0"
    NodeGroupName = "System"
  }

  template_body = <<STACK
Parameters:

  ClusterName:
    Type: String
    Description: The cluster name provided when the cluster was created. If it is incorrect, nodes will not be able to join the cluster.

  KeyName:
    Type: "AWS::EC2::KeyPair::KeyName"
    Description: The EC2 Key Pair to allow SSH access to the instances

  NodeAutoScalingGroupDesiredCapacity:
    Type: Number
    Default: 3
    Description: Desired capacity of Node Group ASG.

  NodeAutoScalingGroupMaxSize:
    Type: Number
    Default: 4
    Description: Maximum size of Node Group ASG. Set to at least 1 greater than NodeAutoScalingGroupDesiredCapacity.

  NodeAutoScalingGroupMinSize:
    Type: Number
    Default: 1
    Description: Minimum size of Node Group ASG.

  NodeGroupName:
    Type: String
    Description: Unique identifier for the Node Group.

  NodeInstanceType:
    Type: String
    Default: t3.medium

  KubernetesVersion:
    Type: String
    Default: '1.19'
    Description: Version of the Kubernetes Control Plane.

  Subnets:
    Type: "List<AWS::EC2::Subnet::Id>"
    Description: The subnets where workers can be created.

Mappings:

  PartitionMap:
    aws:
      EC2ServicePrincipal: "ec2.amazonaws.com"
    aws-us-gov:
      EC2ServicePrincipal: "ec2.amazonaws.com"
    aws-cn:
      EC2ServicePrincipal: "ec2.amazonaws.com.cn"
    aws-iso:
      EC2ServicePrincipal: "ec2.c2s.ic.gov"
    aws-iso-b:
      EC2ServicePrincipal: "ec2.sc2s.sgov.gov"

Resources:

  NodeInstanceRole:
    Type: "AWS::IAM::Role"
    Properties:
      AssumeRolePolicyDocument:
        Version: "2012-10-17"
        Statement:
          - Effect: Allow
            Principal:
              Service:
                - !FindInMap [PartitionMap, !Ref "AWS::Partition", EC2ServicePrincipal]
            Action:
              - "sts:AssumeRole"
      ManagedPolicyArns:
        - !Sub "arn:$${AWS::Partition}:iam::aws:policy/AmazonEKSWorkerNodePolicy"
        - !Sub "arn:$${AWS::Partition}:iam::aws:policy/AmazonEKS_CNI_Policy"
        - !Sub "arn:$${AWS::Partition}:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
      Path: /

  EKSNodegroup:
    Type: 'AWS::EKS::Nodegroup'
    Properties:
      CapacityType: 'SPOT'
      ClusterName: !Ref ClusterName
      InstanceTypes:
        - !Ref NodeInstanceType
      NodeRole: !GetAtt NodeInstanceRole.Arn
      RemoteAccess:
        Ec2SshKey: !Ref KeyName
      ScalingConfig:
        MinSize: !Ref NodeAutoScalingGroupMinSize
        DesiredSize: !Ref NodeAutoScalingGroupDesiredCapacity
        MaxSize: !Ref NodeAutoScalingGroupMaxSize
      Subnets: !Ref Subnets
      
Outputs:

  NodeInstanceRole:
    Description: The node instance role
    Value: !GetAtt NodeInstanceRole.Arn

STACK
  capabilities = [ "CAPABILITY_IAM" ]
  depends_on = [
    aws_cloudformation_stack.eks-control-plane
  ]
}